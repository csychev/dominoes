package dominoes;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class DominoHighLowSetImpl_Sychev implements Domino {
	
	private Set<Integer> highLowSet;
	public static final char SUM_DIFFERENCE_DELIMITER= ',';
	
	public static void main() {
		System.out.println(isSumDifferenceString("6,3"));
	}
	
	public DominoHighLowSetImpl_Sychev(int highPipCount, int lowPipCount) {
		
		highLowSet = new HashSet<Integer>();
		assert highPipCount >= Domino.MINIMUM_PIP_COUNT && highPipCount <=Domino.MAXIMUM_PIP_COUNT : "Invalid";
		assert lowPipCount >= Domino.MINIMUM_PIP_COUNT && lowPipCount <=Domino.MAXIMUM_PIP_COUNT : "Invalid";
		assert highPipCount>=lowPipCount : "invalid";
		
		 
		
		highLowSet.add(highPipCount);
		highLowSet.add(lowPipCount);
		
	}
	
	public static boolean isSumDifferenceString(String str) {
		boolean isSumDifference = true;
		int firstPip = 0;
		int secondPip = 0;
		
		int maxSum = Domino.MAXIMUM_PIP_COUNT * 2; 
		int minSum = Domino.MINIMUM_PIP_COUNT * 2;
		int maxDifference = Domino.MAXIMUM_PIP_COUNT - Domino.MINIMUM_PIP_COUNT;
		int minDifference = Domino.MAXIMUM_PIP_COUNT - Domino.MAXIMUM_PIP_COUNT;
		String delimeter = String.valueOf(SUM_DIFFERENCE_DELIMITER);

		try {
			String[] pipsInArray = str.split(delimeter);
			firstPip = Integer.valueOf(pipsInArray[0]);
			secondPip = Integer.valueOf(pipsInArray[1]);
		}
		catch(Exception e) {
			return false;
		}
		if(!(str.length()<=4 && str.length() >=3)) {
			isSumDifference = false;
			
		}
		

		if(!(firstPip>=secondPip)) {
			isSumDifference = false;
		}
		boolean firstPipRange = (firstPip<=maxSum && firstPip>=minSum);
		if(!firstPipRange) {
			isSumDifference = false;
		}
		boolean secondPipRange = (secondPip>=minDifference && secondPip<=maxDifference);
		if(!secondPipRange) {
			isSumDifference = false;
		}
		return isSumDifference;
		
	}
	
	public DominoHighLowSetImpl_Sychev(String sumDifferenceString) {
		highLowSet = new HashSet<Integer>();
		int firstPip = 0;
		int secondPip = 0;
		String delimeter = String.valueOf(SUM_DIFFERENCE_DELIMITER);
		assert isSumDifferenceString(sumDifferenceString) : "not a sum difference string!";
		String[] sumAndDifference = sumDifferenceString.split(delimeter);
		int sum = Integer.valueOf(sumAndDifference[0]);
		int diff = Integer.valueOf(sumAndDifference[1]);
		firstPip = (sum-diff)/2;
		secondPip = -(sum-diff)/2 + sum;
		assert firstPip + secondPip == sum : "invalid";
		assert Math.abs(firstPip - secondPip) == diff : "invalid";
		assert firstPip >= Domino.MINIMUM_PIP_COUNT && firstPip <= Domino.MAXIMUM_PIP_COUNT : "Invalid";
		assert secondPip >= Domino.MINIMUM_PIP_COUNT && secondPip <= Domino.MAXIMUM_PIP_COUNT : "invalid";
		
		highLowSet.add(firstPip);                                                                              
		highLowSet.add(secondPip);
		
	}
	
	public static boolean isLowPlus8TimesHighInteger(int k) {
		boolean isLowPEightTHigh = true;
		int tempPip = 0;
		Set<Integer> validPips = new HashSet<Integer>();
		for(int low = Domino.MINIMUM_PIP_COUNT; low<=Domino.MAXIMUM_PIP_COUNT; low++) {
			for(int high = low; high<=Domino.MAXIMUM_PIP_COUNT; high++) {
				tempPip = low + (8*high);
				validPips.add(tempPip);
			}
		}
		if(!(validPips.contains(k))) {
			isLowPEightTHigh = false;
		}
		int high = k/8;
		int low = k%8;
		boolean highLowWithinRange = ((high>=Domino.MINIMUM_PIP_COUNT || high<=Domino.MAXIMUM_PIP_COUNT) && (low>=Domino.MINIMUM_PIP_COUNT || low<=Domino.MAXIMUM_PIP_COUNT));
		if(!highLowWithinRange) {
			isLowPEightTHigh = false;
		}
		
		return isLowPEightTHigh;
		
	}

	
	public DominoHighLowSetImpl_Sychev(int lowPlusEightTimesHigh) {
		highLowSet = new HashSet<Integer>();
		assert isLowPlus8TimesHighInteger(lowPlusEightTimesHigh) : "Not valid";
		int low = lowPlusEightTimesHigh%8;
		int high = lowPlusEightTimesHigh/8;
		highLowSet.add(high);
		highLowSet.add(low);
	}

	@Override
	public int getHighPipCount() {
		
		
		int[] pips = getPips();
		
		return pips[1];
		
	}

	@Override
	public int getLowPipCount() {
		
		
		int pips[] = getPips();
		
		return pips[0];
	}
	
	public int[] getPips() {
		int[] myPips = new int[2];
		int loc = 0;
		int count = 0;
		for(int k: highLowSet) {
			myPips[loc++] = k;
			count++;
		}
		if(count==1) {
			int pip = myPips[0];
			myPips[1] = pip;
		}
		
		Arrays.sort(myPips);
		return myPips;
	}
}
