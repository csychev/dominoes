package dominoes;

import java.util.Arrays;
import java.util.List;

public class DominoLowDifferenceStringImpl_Sychev implements Domino{
	
	private String lowDifferenceString = "";
	private static final char LOW_DIFFERENCE_DELIMETER = '*';
	public static final int INDEX_OF_HIGH = 0;
	public static final int INDEX_OF_LOW = 0;
	
	public DominoLowDifferenceStringImpl_Sychev(int lowPlus8TimesHigh) {
		lowDifferenceString = "";
		Domino domino = new DominoHighLowSetImpl_Sychev(lowPlus8TimesHigh);
		int low = domino.getLowPipCount();
		int high = domino.getHighPipCount();
		int diff = high - low;
		String diffStr = Integer.toString(diff);
		String lowStr = Integer.toString(low);
		
		
		lowDifferenceString += lowStr + LOW_DIFFERENCE_DELIMETER + diffStr;
		
		
	}
	
	public DominoLowDifferenceStringImpl_Sychev(List<Integer> highSum) {
		int high = 0;
		int sum = 0;
		assert highSum != null : "invalid";
		assert highSum.get(0) != null : "invalid";
		if (highSum.size() == 2)
			assert highSum.get(1) != null : "Invalid";
		assert highSum.size()==2 : "Invalid";
		high = highSum.get(0);
		sum = highSum.get(1);
		assert !(high>sum) : "Invalid";
		assert high<=Domino.MAXIMUM_PIP_COUNT : "Ivvalid";
		assert high>=Domino.MINIMUM_PIP_COUNT : "Invalid";
		assert sum <= Domino.MAXIMUM_PIP_COUNT + Domino.MAXIMUM_PIP_COUNT;
		assert sum >= Domino.MINIMUM_PIP_COUNT + Domino.MINIMUM_PIP_COUNT;
		//assert (sum< Domino.MAXIMUM_PIP_COUNT + Domino.MAXIMUM_PIP_COUNT) || (sum )
		int low = sum - high;
		int diff = high -low;
		assert low<=Domino.MAXIMUM_PIP_COUNT : "Ivvalid";
		assert low>=Domino.MINIMUM_PIP_COUNT : "Invalid";
		assert diff <= Domino.MAXIMUM_PIP_COUNT - Domino.MINIMUM_PIP_COUNT;
		assert diff >=Domino.MINIMUM_PIP_COUNT - Domino.MINIMUM_PIP_COUNT;
		
		
		String l = Integer.toString(low);
		String d = Integer.toString(diff);
		
		lowDifferenceString += l + LOW_DIFFERENCE_DELIMETER + d;
	}

	@Override
	public int getHighPipCount() {
		String s_low = "";
		String s_Diff = "";
		char l = lowDifferenceString.charAt(0);
		char d = lowDifferenceString.charAt(2);
		s_low += l;
		int low = Integer.parseInt(s_low);
		s_Diff += d;
		int diff = Integer.parseInt(s_Diff);
		int high =  low + diff;
		return high;
	}

	@Override
	public int getLowPipCount() {
		// TODO Auto-generated method stub
		String s_low = "";
		char l = lowDifferenceString.charAt(0);
		s_low += l;
		int low = Integer.parseInt(s_low);
		
		return low;
	}
	


}
