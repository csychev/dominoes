package dominoes;
import java.util.Set;

public class DominoHighLowImpl_Sychev implements Domino{
	
	
	public static final char HIGH_LOW_STRING_SEPARATOR = ':';
	public static final int index_of_sum = 0;
	public static final int index_of_difference = 1;
	private int highPipCount;
	private int lowPipCount;
	
	
	public static void main() {
		System.out.println("HIGH LOW STRING TEST :: " + isHighLowString("3:5"));
	}
	
	
	public DominoHighLowImpl_Sychev(int highPip, int lowPip) {
		
		assert highPip<=MAXIMUM_PIP_COUNT : "Invalid";
		assert lowPip>=MINIMUM_PIP_COUNT : "Invalid";
		
		if(highPip>=lowPip) {
			highPipCount = highPip;
			lowPipCount = lowPip;
			}
		else {
			
			highPipCount = lowPip;
			lowPipCount = highPip;
			
		}
		
	}
	
	
	//POST: Returns string of length 3
	//      Has colon at index 1
	//      High pip_count at index 0
	//      Low pip count at index 2
	public static boolean isHighLowString(String str) {
		if (str == null) {
			return false;
		}
		int firstPip = 0;
		int secondPip = 0;
		String[] thePips = str.split(String.valueOf(HIGH_LOW_STRING_SEPARATOR));
		if (!str.contains(String.valueOf(HIGH_LOW_STRING_SEPARATOR)) || thePips.length != 2) {
			return false;
		}
		firstPip = Integer.valueOf(thePips[0]);
		secondPip = Integer.valueOf(thePips[1]);
		boolean highLowStrTrue = true;
		int myStrLength = str.length();
		boolean highThenLowOrEQ = (firstPip > secondPip || firstPip == secondPip);
		boolean betweenOneAndSix = ((firstPip >= Domino.MINIMUM_PIP_COUNT) && (firstPip <= Domino.MAXIMUM_PIP_COUNT) && (secondPip >= Domino.MINIMUM_PIP_COUNT)&&(secondPip<=Domino.MAXIMUM_PIP_COUNT));
		
		if(myStrLength>3) {
			highLowStrTrue = false; 
		}
		if(!betweenOneAndSix) {
			highLowStrTrue = false;	
		}
		if(!highThenLowOrEQ) {
			highLowStrTrue = false;
		}
		return highLowStrTrue;
	}
	
	public DominoHighLowImpl_Sychev(String highLowString) {
		String delimeter = String.valueOf(HIGH_LOW_STRING_SEPARATOR);
		
		
		assert isHighLowString(highLowString) : "Invalid";
		String[] thePips = highLowString.split(delimeter);
		int theHighPip = Integer.parseInt(thePips[0]);
		int theLowPip = Integer.parseInt(thePips[1]);
		lowPipCount = theLowPip;
		highPipCount = theHighPip;
	}
	
	public DominoHighLowImpl_Sychev(int[] sumDifference) {
		int sum = 0;
		int difference = 0;
		int firstPip;
		int secondPip;
		assert sumDifference != null : "invalid";
		assert sumDifference.length == 2 : "invalid";
		
		
		sum = sumDifference[0];
		difference = sumDifference[1];
		
		assert sum>=(Domino.MINIMUM_PIP_COUNT+Domino.MINIMUM_PIP_COUNT) : "invalid";
		assert sum<=(Domino.MAXIMUM_PIP_COUNT + Domino.MAXIMUM_PIP_COUNT) : "invalid";
		assert difference>=(Domino.MINIMUM_PIP_COUNT - Domino.MINIMUM_PIP_COUNT): "invalid";
		assert difference <= (Domino.MAXIMUM_PIP_COUNT - Domino.MINIMUM_PIP_COUNT) : "invalid";

		assert difference<=sum : "invalid";
		
		firstPip = (sum-difference)/2;
		secondPip = -(sum-difference)/2 + sum;
		
		assert firstPip+secondPip==sum: "invalid";
		assert Math.abs(firstPip-secondPip) == difference : "invalid";
		assert firstPip >= Domino.MINIMUM_PIP_COUNT && firstPip <= Domino.MAXIMUM_PIP_COUNT : "Invalid";
		assert secondPip >= Domino.MINIMUM_PIP_COUNT && secondPip <= Domino.MAXIMUM_PIP_COUNT : "invalid";
		
		if(firstPip>=secondPip) {
			highPipCount = firstPip;
			lowPipCount = secondPip;
		}
		else {
			highPipCount = secondPip;
			lowPipCount = firstPip; 
		}
		
		
	}
	
	public DominoHighLowImpl_Sychev(Set<Integer> highLowSet) {
		assert highLowSet != null : "Invalid";
		Object[] bothPips = highLowSet.toArray();
		int pipOne = 0;
		int pipTwo = 0;
		assert bothPips.length == 2 || bothPips.length == 1 : "Invalid";
		assert bothPips[0] != null : "invalid";
		if (bothPips.length == 2)
			assert bothPips[1] != null : "Invalid";
		if(bothPips.length == 2) {
			pipOne = (int) bothPips[0];
			pipTwo = (int) bothPips[1];
		}
		else if(bothPips.length == 1) {
			pipOne = (int) bothPips[0];
			pipTwo = (int) bothPips[0];
		}
		assert pipOne >= Domino.MINIMUM_PIP_COUNT && pipOne <= Domino.MAXIMUM_PIP_COUNT : "Invalid";
		assert pipTwo >= Domino.MINIMUM_PIP_COUNT && pipTwo <= Domino.MAXIMUM_PIP_COUNT : "invalid";

		if(pipOne>=pipTwo) {
			highPipCount = pipOne;
			lowPipCount = pipTwo;
		}
		
		else {
			highPipCount = pipTwo;
			lowPipCount = pipOne;
		}
		
	}
	public DominoHighLowImpl_Sychev(int highPipDivByTwo, int highPipDivByThreeRemainder, 
									int lowPipDivByTwo,int lowPipDivByThreeRemainder)
	{
		assert lowPipDivByTwo * 2 >= MINIMUM_PIP_COUNT && lowPipDivByTwo * 2 <= MAXIMUM_PIP_COUNT : "invalid";
		assert lowPipDivByThreeRemainder >= 0 && lowPipDivByThreeRemainder < 3: "invalid";
		

		assert highPipDivByTwo * 2 >= MINIMUM_PIP_COUNT && highPipDivByTwo * 2 <= MAXIMUM_PIP_COUNT : "Invalid";
		assert highPipDivByThreeRemainder >= 0 && highPipDivByThreeRemainder < 3: "Invalid";
		

		int high = -1;
		int low = -1;
		

		for(int i = MINIMUM_PIP_COUNT; i <= MAXIMUM_PIP_COUNT; i++)
		{
			if(i / 2 == highPipDivByTwo && i % 3 == highPipDivByThreeRemainder)
			{
				high = i;
			}
		}
		for(int i = MINIMUM_PIP_COUNT; i <= MAXIMUM_PIP_COUNT; i++)
		{
			if(i / 2 == lowPipDivByTwo && i % 3 == lowPipDivByThreeRemainder)
			{
				low = i;
			}
		}
		assert high != -1 : "invalid";
		assert low != -1 : "invalid";
		assert low <= high : "invalid";
		highPipCount = high;
		lowPipCount = low;
	}



	@Override
	public int getHighPipCount() {
		// TODO Auto-generated method stub
		return highPipCount;
	}



	@Override
	public int getLowPipCount() {
		// TODO Auto-generated method stub
		return lowPipCount;
	}
	
	

	

}
